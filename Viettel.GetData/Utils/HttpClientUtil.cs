﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Viettel.GetData.Utils
{
    public class HttpClientUtil
    {
        public static async Task<string> callAPI(HttpClient client, HttpMethod method, string url, String content)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent("Init content response");
            var request = new HttpRequestMessage
            {
                Method = method,
                RequestUri = new Uri(url),
                Content = new StringContent(content, Encoding.UTF8, "application/json")
            };
            try
            {
                response = await client.SendAsync(request);
            }
            catch (Exception)
            {
            }
            var loginResponseString = await response.Content.ReadAsStringAsync();
            return loginResponseString;
        }
    }
}
