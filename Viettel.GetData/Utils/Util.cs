﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Diagnostics;
using System.Windows.Forms;
using OpenQA.Selenium.Internal;

namespace Viettel.GetData.Utils
{
    public static class Util
    {
        public static IWebDriver CreateDriver(bool isHeadless = true)
        {
            ProcessClose("geckodriver");
            //new DriverManager().SetUpDriver(new FirefoxConfig());

            FirefoxDriverService service = FirefoxDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;
            FirefoxOptions options = new FirefoxOptions();
            if (isHeadless)
            {
                // an cua so chrome neu ko phai debug
                options.AddArgument("headless");
            }
            FirefoxProfile profile = new FirefoxProfile();
            profile.AddExtension("Extensions\\FF4_ModifyHeaderWin-2.7.0.xpi");
            profile.AddExtension("Extensions\\viettelhttphandle.xpi");
            options.Profile = profile;
            options.AcceptInsecureCertificates = true;

            return new FirefoxDriver(service, options, TimeSpan.FromSeconds(60.0));
        }

        public static void SelectComboByValue(this IWebElement IWebElement, string value)
        {
            IWebElement.ScrollToView();
            new SelectElement(IWebElement).SelectByValue(value);
        }

        public static void ScrollToView(this IWebElement element)
        {
            IWebDriver wrappedDriver = ((IWrapsDriver)element).WrappedDriver;
            if (element.Location.Y <= 200)
                return;
            wrappedDriver.ScrollTo(yPosition: element.Location.Y - 100);
        }

        public static void ClickFix(this IWebElement element)
        {
            element.ScrollToView();
            element.Click();
        }

        public static void ScrollTo(this IWebDriver driver, int xPosition = 0, int yPosition = 0)
        {
            string script = string.Format("window.scrollTo({0}, {1})", (object)xPosition, (object)yPosition);
            driver.ExecuteScript(script);
        }

        public static object ExecuteScript(this IWebDriver driver, string script) => ((IJavaScriptExecutor)driver).ExecuteScript(script);
        public static object ExecuteScript(this IWebDriver driver, string script, params object[] args) => ((IJavaScriptExecutor)driver).ExecuteScript(script, args);

        public static void WriteNotify(this TextBox tbLog, string message)
        {
            if (tbLog.InvokeRequired)
            {
                tbLog.Invoke(new Action(() =>
                {
                    tbLog.AppendText($"[{DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}]: {message}\r\n");
                    tbLog.ScrollToCaret();
                }));
            }
            else
            {
                tbLog.AppendText($"[{DateTime.Now.ToString("HH:mm:ss.ff")}]: {message}\r\n");
                tbLog.ScrollToCaret();
            }
        }
        public static void WriteLbCheck(this Label lbCheck, string count)
        {
            if (lbCheck.InvokeRequired)
            {
                lbCheck.Invoke(new Action(() =>
                {
                    lbCheck.Text = $"Hôm nay đã tra cứu được {count} thuê bao";
                }));
            }
            else
            {
                lbCheck.Text = $"Hôm nay đã tra cứu được {count} thuê bao";
            }
        }

        public static int NumValue(this TextBox tbDelay)
        {
            try
            {
                return Int32.Parse(tbDelay.Text);
            }
            catch (Exception)
            {
                return 1;
            }
        }

        public static void Enabled(this Button btn, bool status)
        {
            if (btn.InvokeRequired)
            {
                btn.Invoke(new Action(() => btn.Enabled = status));
            }
            else
            {
                btn.Enabled = status;
            }
        }

        public static int Rand(int min, int max)
        {
            Random random = new Random();
            var randomVal = (max - min)/2;
            if (min < randomVal) min = randomVal;
            return random.Next(min, max);
        }

        public static void ProcessClose(string name)
        {
            bool flag;
            do
            {
                flag = false;
                Process[] processes = Process.GetProcesses();
                for (int i = 0; i < processes.Length; i++)
                {
                    try
                    {
                        if (processes[i].ProcessName == name)
                        {
                            processes[i].Kill();
                            flag = true;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            while (flag);
        }

        public static bool IsWithinWorkingHours(DateTime currentTime)
        {
            TimeSpan startMorningShift = new TimeSpan(8, 0, 0);
            TimeSpan endMorningShift = new TimeSpan(11, 30, 0);
            TimeSpan startAfternoonShift = new TimeSpan(13, 30, 0);
            TimeSpan endAfternoonShift = new TimeSpan(18, 0, 0);

            TimeSpan current = currentTime.TimeOfDay;

            return (current >= startMorningShift && current <= endMorningShift) ||
                   (current >= startAfternoonShift && current <= endAfternoonShift);
        }

        public static DateTime CalculateNextWorkingTime(DateTime currentTime)
        {
            TimeSpan current = currentTime.TimeOfDay;

            if (current < new TimeSpan(8, 0, 0))
            {
                // Nếu thời gian hiện tại < 8:00 sáng, thì chờ đến 8:00 sáng
                return currentTime.Date.Add(new TimeSpan(8, 0, 0));
            }
            else if (current > new TimeSpan(11, 30, 0) && current < new TimeSpan(13, 30, 0))
            {
                // Nếu thời gian hiện tại > 11:30 sáng và < 13:30 chiều, thì chờ đến 13:30 chiều
                return currentTime.Date.Add(new TimeSpan(13, 30, 0));
            }
            else if (current > new TimeSpan(18, 0, 0))
            {
                // Nếu thời gian hiện tại > 18:00 tối, thì chờ đến 8:00 sáng hôm sau
                return currentTime.Date.AddDays(1).Add(new TimeSpan(8, 0, 0));
            }
            return currentTime;
        }
    }
}
