﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Viettel.GetData.Utils
{
    public class FileHelper
    {
        public string PathTemp = Path.GetTempPath();

        public string PathFolderExe = Application.StartupPath;

        public string PathFileExe = Application.ExecutablePath;

        public string User = Environment.UserName;

        public string PathPictures = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

        public string PathVideos = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);

        public string PathLocal = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

        public string ExtensionImage = ".jpg .png .gif .bmp .tif";

        private object lockwrite = new object();

        private object lockdeletefile = new object();

        public long GetSize(string pathFile)
        {
            return new FileInfo(pathFile).Length;
        }

        public string ReplaceFilename(string filename)
        {
            filename = AllDecode(filename);
            string text = "";
            text = filename.Replace("\\", "");
            text = text.Replace("/", "");
            text = text.Replace(":", "");
            text = text.Replace("*", "");
            text = text.Replace("?", "");
            text = text.Replace("\"", "");
            text = text.Replace("<", "");
            text = text.Replace(">", "");
            return text.Replace("|", "");
        }

        public static string AllDecode(string data)
        {
            data = Regex.Unescape(data);
            data = Regex.Unescape(data);
            data = Regex.Unescape(data);
            data = Uri.UnescapeDataString(data);
            data = WebUtility.HtmlDecode(data);
            data = data.Replace("&#8211;", "-");
            data = data.Replace("'", "\"");
            data = data.Replace("â€“Â ", "-");
            data = data.Replace("â€œ", "\"");
            return data;
        }

        public void Open(string path)
        {
            path = Path.GetFullPath(path);
            CreateDirectory(path);
            FileStream fileStream = new FileStream(path, FileMode.OpenOrCreate);
            fileStream.Close();
            try
            {
                string fileName = "C:\\Program Files (x86)\\Notepad++\\Notepad++.exe";
                Process.Start(fileName, path);
            }
            catch
            {
                Process.Start(path);
            }
        }

        public string Read(string path)
        {
            CreateDirectory(path);
            string result = "";
            try
            {
                FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using (StreamReader streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    result = streamReader.ReadToEnd();
                }

                fileStream.Close();
            }
            catch
            {
            }

            return result;
        }

        public List<string> ReadLine(string path, string textLoc = "", bool PhanBietHoaThuong = false)
        {
            List<string> list = new List<string>();
            CreateDirectory(path);
            try
            {
                string text = Read(path);
                string[] array = text.Split(new string[3] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                for (int i = 0; i < array.Length; i++)
                {
                    if (!(array[i] != ""))
                    {
                        continue;
                    }

                    if (textLoc != "")
                    {
                        RegexOptions options = RegexOptions.None;
                        if (!PhanBietHoaThuong)
                        {
                            options = RegexOptions.IgnoreCase;
                        }

                        if (Regex.IsMatch(array[i], textLoc, options))
                        {
                            list.Add(array[i]);
                        }
                    }
                    else
                    {
                        list.Add(array[i]);
                    }
                }
            }
            catch
            {
            }

            return list;
        }

        public string ReadLine(string path, int line)
        {
            string result = "";
            CreateDirectory(path);
            try
            {
                string text = Read(path);
                string[] array = text.Split('\n');
                result = array[line];
            }
            catch
            {
            }

            return result;
        }

        public List<string> GetFiles(string thumuc, string[] duoifile)
        {
            List<string> list = new List<string>();
            try
            {
                string[] files = Directory.GetFiles(thumuc);
                for (int i = 0; i < files.Length; i++)
                {
                    string text = files[i].Replace("/", "\\");
                    if (duoifile == null)
                    {
                        list.Add(text);
                        continue;
                    }

                    string extension = Path.GetExtension(text);
                    if (duoifile.Contains(extension))
                    {
                        list.Add(text);
                    }
                }
            }
            catch
            {
            }

            return list;
        }

        public void DeleteDirTemp()
        {
            try
            {
                string[] directories = Directory.GetDirectories(Path.GetTempPath());
                for (int i = 0; i < directories.Length; i++)
                {
                    try
                    {
                        Directory.Delete(directories[i], recursive: true);
                    }
                    catch
                    {
                    }
                }

                string[] files = Directory.GetFiles(Path.GetTempPath());
                for (int j = 0; j < files.Length; j++)
                {
                    try
                    {
                        File.Delete(files[j]);
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
            }
        }

        public void Write(string path, string noidung, bool readPath = false, bool writetofile = true)
        {
            lock (lockwrite)
            {
                CreateDirectory(path);
                string text = "";
                if (readPath)
                {
                    try
                    {
                        text = File.ReadAllText(path);
                    }
                    catch
                    {
                    }
                }

                noidung = noidung + "\r\n" + text;
                while (true)
                {
                    try
                    {
                        File.WriteAllText(path, noidung, Encoding.UTF8);
                        return;
                    }
                    catch
                    {
                    }

                    Thread.Sleep(100);
                }
            }
        }

        public void Write(string path, TextBox textbox, bool writetofile = true)
        {
            lock (lockwrite)
            {
                if (!writetofile)
                {
                    try
                    {
                        textbox.Text = File.ReadAllText(path);
                    }
                    catch
                    {
                    }
                }
                else
                {
                    CreateDirectory(path);
                    string text = textbox.Text;
                    File.WriteAllText(path, text, Encoding.UTF8);
                }
            }
        }

        public void DeleteLineDuplicate(string filename)
        {
            lock (lockwrite)
            {
                try
                {
                    string[] source = File.ReadAllLines(filename);
                    File.WriteAllLines(filename, source.Distinct().ToArray());
                }
                catch
                {
                }
            }
        }

        public void WriteLine(string path, string noidung)
        {
            lock (lockwrite)
            {
                CreateDirectory(path);
                string text = "";
                try
                {
                    text = File.ReadAllText(path);
                }
                catch
                {
                }

                if (text != "")
                {
                    text += "\r\n";
                }

                noidung = text + noidung;
                while (true)
                {
                    try
                    {
                        File.WriteAllText(path, noidung, Encoding.UTF8);
                        return;
                    }
                    catch
                    {
                    }

                    Thread.Sleep(100);
                }
            }
        }

        public bool Delete(string file)
        {
            try
            {
                File.Delete(file);
                return true;
            }
            catch
            {
            }

            return false;
        }

        public void DeleteTextSeacrh(string pathfile, string textDelete)
        {
            lock (lockdeletefile)
            {
                try
                {
                    string[] contents = (from line in File.ReadAllLines(pathfile)
                                         where line.Trim() != textDelete
                                         select line).ToArray();
                    File.WriteAllLines(pathfile, contents);
                }
                catch
                {
                }
            }
        }

        public void DeleteLineEmpty(string pathfile)
        {
            lock (lockdeletefile)
            {
                File.WriteAllLines(pathfile, from l in File.ReadAllLines(pathfile)
                                             where !string.IsNullOrWhiteSpace(l)
                                             select l);
            }
        }

        public void DeleteTextSeacrhContains(string pathfile, string textDelete)
        {
            lock (lockdeletefile)
            {
                try
                {
                    string[] contents = (from line in File.ReadAllLines(pathfile)
                                         where !line.Trim().ToLower().Contains(textDelete.ToLower().Trim())
                                         select line).ToArray();
                    File.WriteAllLines(pathfile, contents);
                }
                catch
                {
                }
            }
        }

        public void CreateDirectory(string path)
        {
            path = Path.GetDirectoryName(path);
            if (path == "")
            {
                return;
            }

            if (Path.GetExtension(path) == "")
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                return;
            }

            path = Path.GetDirectoryName(path);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public void CopyDirectory(string sourceDir, string targetDir)
        {
            try
            {
                if (!Directory.Exists(targetDir))
                {
                    Directory.CreateDirectory(targetDir);
                }

                string[] files = Directory.GetFiles(sourceDir);
                foreach (string text in files)
                {
                    try
                    {
                        File.Copy(text, Path.Combine(targetDir, Path.GetFileName(text)), overwrite: true);
                    }
                    catch
                    {
                    }
                }

                string[] directories = Directory.GetDirectories(sourceDir);
                foreach (string text2 in directories)
                {
                    CopyDirectory(text2, Path.Combine(targetDir, Path.GetFileName(text2)));
                }
            }
            catch
            {
            }
        }
    }
}
