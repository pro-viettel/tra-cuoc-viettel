﻿using NLog;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Data;
using OpenQA.Selenium.Firefox;
using System.Drawing.Printing;
using System.Net.Http;
using DeviceId;
using Viettel.GetData.Utils;
using Viettel.GetData.Configs;
using System.Net;
using Newtonsoft.Json.Linq;

namespace Viettel.GetData
{
    public partial class Form1 : Form
    {
        Logger _logger = LogManager.GetLogger("myLoggerRules");
        IWebDriver firefoxDriver = (FirefoxDriver)null;
        public FileHelper file = new FileHelper();
        public string tab = "\t";
        private String APP_KEY = "";


        public Form1()
        {
            InitializeComponent();
            this.Text = $"{AppConfig.APP_NAME} {AppConfig.APP_VERSION}";
            // set giá trị cho combo xem file
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("ID", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));

            dataTable.Rows.Add(1, "Danh sách ban đầu");
            dataTable.Rows.Add(2, "Thông tin khách hàng");
            dataTable.Rows.Add(3, "Dịch vụ liên quan");
            dataTable.Rows.Add(4, "Cước đóng trước");
            dataTable.Rows.Add(5, "Cước đóng hàng tháng");
            dataTable.Rows.Add(6, "Danh sách đã check");

            this.cbFile.DataSource = dataTable;
            this.cbFile.ValueMember = "ID";
            this.cbFile.DisplayMember = "Name";

            FillCbDichVu();
            FillPassword();
            BindHeaderFile();
            InitCheckedFile();

            cbFile.SelectedIndex = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                string deviceId = new DeviceIdBuilder()
                .OnWindows(windows => windows
                    .AddProcessorId()
                    .AddMotherboardSerialNumber()
                    .AddSystemDriveSerialNumber())
                .ToString();
                File.WriteAllText("DataFile/key.txt", deviceId);
                APP_KEY = deviceId;
                //HttpClient client = new HttpClient();
                //string content = $@"{{
                //                          ""app"": ""{AppConfig.APP_CODE}_{AppConfig.APP_VERSION}"",
                //                          ""key"": ""{deviceId}""
                //                        }}";
                //var res = await HttpClientUtil.callAPI(client, HttpMethod.Post, "http://xacthuc.provnpt.com/api/v1/auth-app", content);
                //if (res.Contains("\"active\":true"))
                //{
                //    return;
                //}
                if (APP_KEY == "D45M48C09KDMRNSV60T0MQBPQJ7M6DYMNBZV6NQGNHCPA8XDXWG0")
                {
                    return;
                }
                else
                {
                    MessageBox.Show("App chưa được kích hoạt");
                    this.Close();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi license, vui lòng liên hệ dev.");
            }
        }

        public void FillCbDichVu()
        {
            try
            {
                var serviceData = this.file.ReadLine("DataFile/service.txt", "");
                if (serviceData.Count > 0)
                {
                    this.cbService.DataSource = serviceData;
                    cbService.SelectedIndex = 0;
                }
                else
                {
                    this.tbLog.WriteNotify("Chưa có dữ liệu dịch vụ, vui lòng cập nhật!");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public void FillPassword()
        {
            try
            {
                var userData = this.file.ReadLine("user.cfg", "");
                if (userData.Count > 0)
                {
                    this.tbUsername.Text = userData[0];
                    this.tbPassword.Text = userData[1];
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        private void btnViewFile_Click(object sender, EventArgs e)
        {
            try
            {
                int selectedValue = (int)this.cbFile.SelectedValue;
                string filename = "danhsachbandau";
                switch (selectedValue)
                {
                    case 1: filename = "danhsachbandau"; break;
                    case 2: filename = "thongtinkhachhang"; break;
                    case 3: filename = "dichvulienquan"; break;
                    case 4: filename = "cuocdongtruoc"; break;
                    case 5: filename = "cuocdonghangthang"; break;
                    case 6: filename = "danhsachdacheck"; break;
                    default: break;
                }
                this.file.Open($"DataFile/{filename}.txt");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string username = tbUsername.Text;
            string password = tbPassword.Text;
            btnLogin.Enabled(false);

            try
            {
                firefoxDriver?.Quit();
                firefoxDriver = Util.CreateDriver(!this.ckIsDebug.Checked);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                this.tbLog.WriteNotify("Không khởi tạo được driver!");
                this.tbLog.WriteNotify("Dừng chức năng!");
                btnLogin.Enabled(true);
                return;
            }

            new Thread(() =>
            {
                try
                {
                    var url = "http://10.240.147.246/BCCS_CC/";
                    if (!Navigate(url))
                    {
                        this.tbLog.WriteNotify("Thực hiện điều hướng - Fail");
                        this.tbLog.WriteNotify("Dừng chức năng!");
                        btnLogin.Enabled(true);
                        return;
                    }
                    Thread.Sleep(2000);

                    this.tbLog.WriteNotify("Thực hiện đăng nhập");

                    firefoxDriver.FindElement(By.Id("username")).SendKeys(username);
                    firefoxDriver.FindElement(By.Id("password")).SendKeys(password + OpenQA.Selenium.Keys.Enter);

                    Thread.Sleep(5000);

                    for (int i = 0; i < 5; i++)
                    {
                        if (!this.firefoxDriver.PageSource.Contains("Tìm kiếm khách hàng"))
                        {
                            Thread.Sleep(1000);
                            continue;
                        }

                        this.tbLog.WriteNotify("Đăng nhập thành công");
                        string content = "";
                        if (this.ckIsSaveAccount.Checked)
                        {
                            content = $"{this.tbUsername.Text}\r\n{this.tbPassword.Text}";
                        }
                        File.WriteAllText("user.cfg", content);

                        btnSearch.Enabled(true);
                        btnInfo.Enabled(true);
                        btnUpdateService.Enabled(true);
                        btnReset.Enabled(true);
                        return;
                    }
                    this.tbLog.WriteNotify("Đăng nhập thất bại!");
                    this.tbLog.WriteNotify("Hãy thử đăng nhập lại!");
                    btnLogin.Enabled(true);

                }
                catch (Exception ex)
                {
                    this.tbLog.WriteNotify("Đăng nhập thất bại!");
                    btnLogin.Enabled(true);
                    _logger.Error(ex);
                }
            })
            { IsBackground = true }.Start();


        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.tbDelay.Text) || this.tbDelay.NumValue() < 30)
            {
                this.tbLog.WriteNotify("Delay tối thiểu là 30!");
                return;
            }
            btnSearch.Enabled(false);
            btnInfo.Enabled(false);
            btnStop.Enabled(true);
            var url = "http://10.240.147.246/BCCS_CC/";

            if (!Navigate(url))
            {
                this.tbLog.WriteNotify("Thực hiện điều hướng - Fail");
                this.tbLog.WriteNotify("Dừng chức năng!");
                this.btnSearch.Enabled(true);
                btnStop.Enabled(false);
                return;
            }
            new Thread(() =>
            {
                if (!ClickMenuTimKiem())
                {
                    this.tbLog.WriteNotify("Click menu tìm kiếm khách hàng - Fail");
                    this.tbLog.WriteNotify("Dừng chức năng!");
                    btnSearch.Enabled(true);
                    return;
                }
                Thread.Sleep(Util.Rand(3000, 4000));

                if (!DienDiaChi())
                {
                    this.tbLog.WriteNotify("Điền địa chỉ - Fail");
                    this.tbLog.WriteNotify("Dừng chức năng!");
                    btnSearch.Enabled(true);
                    return;
                }
                Thread.Sleep(1000);

                if (!ChonDichVu())
                {
                    this.tbLog.WriteNotify("Chọn dịch vụ - Fail");
                    this.tbLog.WriteNotify("Dừng chức năng!");
                    btnSearch.Enabled(true);
                    return;
                }
                Thread.Sleep(1000);

                if (!ClickTimKiem())
                {
                    this.tbLog.WriteNotify("Click tìm kiếm khách hàng - Fail");
                    this.tbLog.WriteNotify("Dừng chức năng!");
                    btnSearch.Enabled(true);
                    return;
                }
                Thread.Sleep(Util.Rand(6000, 10000));

                if (!GetDanhSach())
                {
                    this.tbLog.WriteNotify("Lấy danh sách khách hàng - Fail");
                    this.tbLog.WriteNotify("Dừng chức năng!");
                    btnSearch.Enabled(true);
                    return;
                }
                btnSearch.Enabled(true);
                btnInfo.Enabled(true);
                btnStop.Enabled(false);
            })
            { IsBackground = true }.Start();
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.tbDelay.Text) || this.tbDelay.NumValue() < 30)
                {
                    this.tbLog.WriteNotify("Delay tối thiểu là 30!");
                    return;
                }

                if (string.IsNullOrEmpty(this.tbLimit.Text))
                {
                    this.tbLog.WriteNotify("Vui lòng nhập giới hạn số tra cứu/ngày!");
                    return;
                }

                var listSoThueBao = new List<string>();
                if (this.rbFile.Checked)
                {
                    List<string> dataFile = this.file.ReadLine("DataFile/danhsachbandau.txt", "");

                    for (int index = 1; index < dataFile.Count; ++index)
                    {
                        var stb = Regex.Match(dataFile[index], "(.*?)\t").Groups[1].Value;
                        if (!listSoThueBao.Contains(stb))
                            listSoThueBao.Add(stb);
                    }
                }
                else if (this.rbInput.Checked)
                {
                    tbListSTB.Text = tbListSTB.Text.Trim();
                    int nLines = tbListSTB.Lines.Length;
                    for (int i = 0; i < nLines; i++)
                    {
                        if (this.tbListSTB.Lines[i] != "")
                            listSoThueBao.Add(this.tbListSTB.Lines[i].Trim());
                    }
                }
                else
                {
                    this.tbLog.WriteNotify("Chưa chọn dữ liệu đầu vào, vui lòng chọn lấy thông tin từ DS ban đầu hoặc ô nhập liệu!");
                }
                if (!listSoThueBao.Any())
                {
                    this.tbLog.WriteNotify("Không thấy dữ liệu từ file hoặc ô nhập liệu!");
                }

                btnSearch.Enabled(false);
                this.btnInfo.Enabled(false);
                btnStop.Enabled(true);


                var url = "http://10.240.147.246/BCCS_CC/";

                new Thread(() =>
                {
                    int count = Int32.Parse(Regex.Match(this.lbChecked.Text, "Hôm nay đã tra cứu được (\\d+) thuê bao").Groups[1].Value.Trim());
                    int limit = Util.Rand(Int32.Parse(this.tbLimit.Text) - 10,Int32.Parse(this.tbLimit.Text) + 10);
                    for (int index = 0; index < listSoThueBao.Count; ++index)
                    {
                        string soThueBao = listSoThueBao[index].Trim();

                        CheckInWorkHour(ref count, ref limit);

                        var dataChecked = this.file.Read("DataFile/danhsachdacheck.txt");

                        if (dataChecked != null && dataChecked.Contains(soThueBao)) continue;

                        if (count >= limit)
                        {
                            this.tbLog.WriteNotify($"Đã tra cứu đủ {limit} khách hàng / ngày.");
                            DateTime currentTime = DateTime.Now;
                            DateTime nextWorkingTime = currentTime.Date.AddDays(1).Add(new TimeSpan(8, 0, 0));
                            TimeSpan sleepDuration = nextWorkingTime - currentTime;
                            string formattedDateTime = nextWorkingTime.ToString("dd/MM/yyyy HH:mm:ss tt");
                            this.tbLog.WriteNotify("Chờ đến " + formattedDateTime + " sẽ tra cứu tiếp.");
                            btnStop.Enabled(false);
                            Thread.Sleep(sleepDuration);
                            btnStop.Enabled(true);
                            count = 0;
                            limit = Util.Rand(Int32.Parse(this.tbLimit.Text) - 10, Int32.Parse(this.tbLimit.Text) + 10);
                        }
                        this.tbLog.WriteNotify("=============================================================");
                        this.tbLog.WriteNotify($"Bắt đầu lấy thông tin/dịch vụ/cước/cước đóng trước - {this.tbDelay.NumValue()} +- {this.tbRandomDelay.NumValue()} giây/user");
                        this.tbLog.WriteNotify("=============================================================");

                        if (!btnStop.Enabled)
                        {
                            this.tbLog.WriteNotify("Stop done!");
                            return;
                        }

                        if (!Navigate(url))
                        {
                            this.tbLog.WriteNotify("Thực hiện điều hướng - Fail");
                            this.tbLog.WriteNotify("Dừng chức năng!");
                            continue;
                        }

                        if (!ClickMenuTimKiem())
                        {
                            this.tbLog.WriteNotify("Click menu tìm kiếm khách hàng - Fail");
                            this.tbLog.WriteNotify("Chuyển sang KH tiếp theo!");
                            continue;
                        }
                        Thread.Sleep(Util.Rand((this.tbDelay.NumValue() * 1000 / 4) - this.tbRandomDelay.NumValue() * 1000, (this.tbDelay.NumValue() * 1000 / 4) + this.tbRandomDelay.NumValue() * 1000));

                        if (!DienSoThueBao(soThueBao))
                        {
                            this.tbLog.WriteNotify("Điền số thuê bao - Fail");
                            this.tbLog.WriteNotify("Chuyển sang KH tiếp theo!");
                            continue;
                        }
                        Thread.Sleep(5000);

                        if (!ClickTimKiem())
                        {
                            this.tbLog.WriteNotify("Click tìm kiếm khách hàng - Fail");
                            this.tbLog.WriteNotify("Chuyển sang KH tiếp theo!");
                            continue;
                        }
                        Thread.Sleep(Util.Rand((this.tbDelay.NumValue() * 1000 / 4) - this.tbRandomDelay.NumValue() * 1000, (this.tbDelay.NumValue() * 1000 / 4) + this.tbRandomDelay.NumValue() * 1000));

                        if (!ClickRowKhachHang(soThueBao))
                        {
                            this.tbLog.WriteNotify("Click xem thông tin - Fail");
                            this.tbLog.WriteNotify("Chuyển sang KH tiếp theo!");
                            continue;
                        }
                        Thread.Sleep(Util.Rand((this.tbDelay.NumValue() * 1000 / 4) - this.tbRandomDelay.NumValue() * 1000, (this.tbDelay.NumValue() * 1000 / 4) + this.tbRandomDelay.NumValue() * 1000));

                        if (!GetThongTin(soThueBao))
                        {
                            this.tbLog.WriteNotify("Lấy thông tin khách hàng - Fail");
                            this.tbLog.WriteNotify("Chuyển sang KH tiếp theo!");
                            continue;
                        }

                        if (!ClickXemCuoc())
                        {
                            this.tbLog.WriteNotify("Click xem cước - Fail");
                            this.tbLog.WriteNotify("Chuyển sang KH tiếp theo!");
                            continue;
                        }
                        Thread.Sleep(Util.Rand((this.tbDelay.NumValue() * 1000 / 4) - this.tbRandomDelay.NumValue() * 1000, (this.tbDelay.NumValue() * 1000 / 4) + this.tbRandomDelay.NumValue() * 1000));

                        string maThueBao = GetMaThueBao(soThueBao);

                        if (!GetCuoc(soThueBao, maThueBao))
                        {
                            this.tbLog.WriteNotify("Lấy cước đóng trước/hàng tháng - Fail");
                            this.tbLog.WriteNotify("Chuyển sang KH tiếp theo!");
                            continue;
                        }
                        File.AppendAllText("DataFile/danhsachdacheck.txt", soThueBao + "\n");
                        count++;
                        SaveCheckedFile(count);
                    }
                    this.tbLog.WriteNotify("Done.");
                    btnSearch.Enabled(true);
                    btnInfo.Enabled(true);
                    btnStop.Enabled(false);
                })
                { IsBackground = true }.Start();
            }
            catch (Exception)
            {
            }
        }

        public void SaveCheckedFile(int count)
        {
            try
            {
                string delay = this.tbDelay.Text.Trim();
                string randomDelay = this.tbRandomDelay.Text.Trim();
                string countChecked = count.ToString();
                string limit = this.tbLimit.Text.Trim();
                string today = DateTime.Today.ToString("dd/MM/yyyy");
                this.lbChecked.WriteLbCheck(countChecked);
                string value = $"Delay:{this.tab}{delay}\nRandomDelay:{this.tab}{randomDelay}\nCountChecked:{this.tab}{countChecked}\nLimit:{this.tab}{limit}\nDatetime:{this.tab}{today}\n";
                File.WriteAllText("checkconfig.cfg", value);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public bool ClickMenuTimKiem()
        {
            this.tbLog.WriteNotify("Click menu tìm kiếm khách hàng");
            int i = 5;
            // loop nếu chưa tìm đc ele thì chạy lại, quá 5 lần thì break
            while (i > 0)
            {
                i--;
                try
                {
                    List<IWebElement> list = firefoxDriver.FindElements(By.TagName("a")).ToList<IWebElement>();
                    for (int index = 0; index < list.Count; ++index)
                    {
                        if (list[index].Text.Trim() == "Tìm kiếm khách hàng")
                        {
                            firefoxDriver.ExecuteScript("arguments[0].click();", new object[1] { (object)list[index] });
                            // pass qua thì return true
                            this.tbLog.WriteNotify("Click menu tìm kiếm khách hàng - OK");
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.tbLog.WriteNotify("Không tìm thấy element!");
                    _logger.Error(ex);
                }
                // chưa pass hoặc exception thì repeat 
                this.tbLog.WriteNotify("Click menu tìm kiếm khách hàng - Repeat");
                Thread.Sleep(5000);

            }
            // hết loop mà chưa return thì return false
            return false;
        }

        public bool DienSoThueBao(string soThueBao)
        {
            this.tbLog.WriteNotify($"Điền số thuê bao: {soThueBao}");
            int i = 5;
            // loop nếu chưa tìm đc ele thì chạy lại, quá 5 lần thì break
            while (i > 0)
            {
                i--;
                try
                {
                    firefoxDriver.FindElement(By.Id("formIn:l2:isdn")).Clear();
                    firefoxDriver.FindElement(By.Id("formIn:l2:isdn")).SendKeys(soThueBao);
                    // pass qua thì return true
                    this.tbLog.WriteNotify("Điền số thuê bao - OK");
                    return true;

                }
                catch (Exception ex)
                {
                    this.tbLog.WriteNotify("Không tìm thấy element!");
                    _logger.Error(ex);
                }
                // chưa pass hoặc exception thì repeat 
                this.tbLog.WriteNotify("Điền số thuê bao - Repeat");
                Thread.Sleep(2000);

            }
            // hết loop mà chưa return thì return false
            return false;
        }

        public bool DienDiaChi()
        {
            this.tbLog.WriteNotify("Điền địa chỉ");
            int i = 5;
            // loop nếu chưa tìm đc ele thì chạy lại, quá 5 lần thì break
            while (i > 0)
            {
                i--;
                try
                {
                    firefoxDriver.FindElement(By.Id("formIn:l2:custAddress")).Clear();
                    firefoxDriver.FindElement(By.Id("formIn:l2:custAddress")).SendKeys(this.tbAddress.Text.Trim());
                    // pass qua thì return true
                    this.tbLog.WriteNotify("Điền địa chỉ - OK");
                    return true;

                }
                catch (Exception ex)
                {
                    this.tbLog.WriteNotify("Không tìm thấy element!");
                    _logger.Error(ex);
                }
                // chưa pass hoặc exception thì repeat 
                this.tbLog.WriteNotify("Điền địa chỉ - Repeat");
                Thread.Sleep(2000);

            }
            // hết loop mà chưa return thì return false
            return false;
        }

        public bool ChonDichVu()
        {
            this.tbLog.WriteNotify("Chọn dịch vụ");
            int i = 5;
            // loop nếu chưa tìm đc ele thì chạy lại, quá 5 thì break
            while (i > 0)
            {
                i--;
                try
                {
                    List<IWebElement> list = firefoxDriver.FindElements(By.TagName("label")).ToList<IWebElement>();
                    for (int index = 0; index < list.Count; ++index)
                    {
                        if (list[index].Text.Trim() == "--Lựa chọn--")
                        {
                            list[index].Click();
                            break;
                        }
                    }

                    List<IWebElement> listDV = firefoxDriver.FindElements(By.TagName("label")).ToList<IWebElement>();
                    for (int index = 0; index < listDV.Count; ++index)
                    {
                        if (listDV[index].Text.Trim() == this.cbService.SelectedItem.ToString())
                        {
                            firefoxDriver.ExecuteScript("arguments[0].click();", new object[1] { (object)listDV[index] });
                            // pass qua thì return true
                            this.tbLog.WriteNotify("Chọn dịch vụ - OK");
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.tbLog.WriteNotify("Không tìm thấy element!");
                    _logger.Error(ex);
                }
                // chưa pass hoặc exception thì repeat 
                this.tbLog.WriteNotify("Chọn dịch vụ - Repeat");
                Thread.Sleep(2000);

            }
            // hết loop mà chưa return thì return false
            return false;
        }

        public bool ClickTimKiem()
        {
            this.tbLog.WriteNotify("Click tìm kiếm khách hàng");
            int i = 5;
            // loop nếu chưa tìm đc ele thì chạy lại, quá 5 thì break
            while (i > 0)
            {
                i--;
                try
                {
                    firefoxDriver.FindElement(By.Id("formIn:l2:searchSubBtn")).Click();
                    this.tbLog.WriteNotify("Click tìm kiếm khách hàng - OK");
                    return true;

                }
                catch (Exception ex)
                {
                    this.tbLog.WriteNotify("Không tìm thấy element!");
                    _logger.Error(ex);
                }
                // chưa pass hoặc exception thì repeat 
                this.tbLog.WriteNotify("Click tìm kiếm khách hàng - Repeat");
                Thread.Sleep(2000);

            }
            // hết loop mà chưa return thì return false
            return false;
        }

        public bool ClickRowKhachHang(string soThueBao)
        {
            this.tbLog.WriteNotify("Click xem thông tin");
            int i = 5;
            // loop nếu chưa tìm đc ele thì chạy lại, quá 5 thì break
            while (i > 0)
            {
                i--;
                try
                {
                    var str = Regex.Match(firefoxDriver.PageSource, "title=\"" + soThueBao + "\"").Groups[0].Value;
                    if (str != "")
                    {
                        firefoxDriver.ExecuteScript("arguments[0].click();",
                        new object[1]
                        {
                            (object) firefoxDriver.FindElement(By.CssSelector("tr.ui-datatable-even:nth-child(1) > td:nth-child(2)"))
                        });
                        this.tbLog.WriteNotify("Click xem thông tin - OK!");
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    this.tbLog.WriteNotify("Không tìm thấy element!");
                    _logger.Error(ex);
                }
                // chưa pass hoặc exception thì repeat 
                this.tbLog.WriteNotify("Click xem thông tin - Repeat");
                Thread.Sleep(2000);

            }
            // hết loop mà chưa return thì return false
            return false;
        }

        public bool GetDanhSach()
        {
            this.tbLog.WriteNotify("Lấy danh sách khách hàng");
            int i = 5;
            this.tbLog.WriteNotify("Chọn combo 50");
            // loop nếu chưa tìm đc ele thì chạy lại, quá 5 thì break
            while (i > 0)
            {
                i--;
                try
                {
                    string totalRecord = Regex.Match(firefoxDriver.PageSource, "<span class=\"ui-paginator-current\">.*? của (\\d+) bản ghi, Trang:.*?</span>").Groups[1].Value;
                    string paging = Regex.Match(firefoxDriver.PageSource, "class=\"ui-paginator-rpp-options.*?selected=\"selected\">(\\d+)</option>").Groups[1].Value;
                    if (paging != "50" || i == 2)
                    {
                        firefoxDriver.FindElement(By.Id("formIn:l2:tableSubInfo_rppDD")).SelectComboByValue("50");
                    }
                    Thread.Sleep(Util.Rand(20000, 25000));
                    // kiểm tra xem có số bản ghi không --- VD: Bản ghi 1 - 10
                    Match match = Regex.Match(firefoxDriver.PageSource, "<span class=\"ui-paginator-current\">.Bản ghi (.*?) - (\\d+)");
                    bool success = match.Success;
                    string prefix = match.Groups[1].Value;
                    string afterfix = match.Groups[2].Value;
                    // nếu có thì break loop
                    if (prefix != "" && (afterfix == "50" || afterfix == totalRecord))
                    {
                        this.tbLog.WriteNotify("Chọn combo 50 - OK");
                        break;
                    }
                }
                catch (Exception ex)
                {
                    this.tbLog.WriteNotify("Không tìm thấy element!");
                    this.tbLog.WriteNotify("Chọn combo 50 - Repeat!");
                    _logger.Error(ex);
                }
                // nếu k có hoặc exception thì sleep
                Thread.Sleep(10000);
            }
            if (i == 0) { this.tbLog.WriteNotify("Chọn combo 50 thất bại!"); return false; }
            try
            {
                // lấy ra tổng số trang
                string s = Regex.Match(firefoxDriver.PageSource, "<span class=\"ui-paginator-current\">.*?, Trang: 1/(\\d+).</span>").Groups[1].Value;
                if (s == "")
                {
                    s = "0";
                }

                int totalPage = int.Parse(s);

                List<MatchCollection> matchCollectionList = new List<MatchCollection>();

                // chạy từng trang
                for (int currentPage = 0; currentPage < totalPage; ++currentPage)
                {
                    if (!btnStop.Enabled)
                    {
                        this.tbLog.WriteNotify("Stop done!");
                        return true;
                    }
                    this.tbLog.WriteNotify("=============================================================");
                    this.tbLog.WriteNotify($"Bắt đầu lấy danh sách khách hàng - khoảng {this.tbDelay.NumValue()} +- {this.tbRandomDelay.NumValue()} giây/trang");
                    this.tbLog.WriteNotify("=============================================================");
                    this.tbLog.WriteNotify("Bắt đầu lấy thông tin trang: " + (object)(currentPage + 1) + "/" + (object)totalPage);
                    // bản ghi thứ n tại page
                    int checkCount = 0;
                    MatchCollection matchCollection = null;
                    while (checkCount < 6)
                    {
                        ++checkCount;

                        // nếu currentPage != trang hiện tại trên web
                        if (Regex.Match(firefoxDriver.PageSource, "ui-paginator-page.*?ui-state-active.*?>(\\d+)<").Groups[1].Value != (currentPage + 1).ToString())
                        {
                            // và số lần check của page hiện tại > 2 thì click sang page sau
                            if (checkCount == 3 || checkCount == 5)
                                firefoxDriver.FindElement(By.CssSelector(".ui-icon-seek-next")).Click();
                            Thread.Sleep(Util.Rand(10000, 20000));
                        }
                        else
                        {
                            // regex lấy ra toàn bộ các dòng của page hiện tại
                            matchCollection = Regex.Matches(firefoxDriver.PageSource, "<tr.*?class=.*?ui-widget-content ui-datatable.*? ui-datatable-selectable.*?><td.*?<span title=.*?>(.*?)</span></td><td.*?<span title=.*?>(.*?)</span></td><td.*?<span title=.*?>(.*?)</span></td><td.*?<span title=.*?>(.*?)</span></td><td.*?<span title=.*?>(.*?)</span></td><td.*?<span title=.*?>(.*?)</span></td><td.*?class=\"ui-outputlabel ui-widget\">(.*?)</label></td><td.*?class=\"ui-outputlabel ui-widget\">(.*?)</label></td><td.*?class=\"ui-outputlabel ui-widget\">(.*?)</label></td><td.*?<span title=.*?>(.*?)</span></td><td.*?>(.*?)</td></tr>");
                            // check tồn tại trong list, nếu đã có thì chạy vòng tiếp theo (trường hợp do page load chưa kịp nên bị dữ liệu cũ)
                            if (matchCollectionList.Contains(matchCollection))
                                Thread.Sleep(Util.Rand(5000, 10000));
                            // nếu k thì break loop để add vào list
                            else
                                break;
                        }
                    }
                    // thêm vào list
                    if (matchCollection == null)
                    {
                        this.tbLog.WriteNotify("Lấy thông tin trang: " + (object)(currentPage + 1) + "/" + (object)totalPage + " thất bại!");
                        this.tbLog.WriteNotify("Thực hiện lấy lại thông tin trang: " + (object)(currentPage + 1) + "/" + (object)totalPage);
                        currentPage = currentPage - 1;
                        continue;
                    }
                    matchCollectionList.Add(matchCollection);
                    var flag = false;
                    for (int row = 0; row < matchCollection.Count; ++row)
                    {
                        // kiểm tra nếu tồn tại id khách hàng rồi thì ko add vào file nữa
                        string soThueBao = matchCollection[row].Groups[1].Value;
                        string maThueBao = matchCollection[row].Groups[2].Value;
                        string dichVu = matchCollection[row].Groups[3].Value;
                        string trangThaiTB = matchCollection[row].Groups[4].Value;
                        string tinhTrangChanCat = matchCollection[row].Groups[5].Value;
                        string loaiSP = matchCollection[row].Groups[6].Value;
                        string tenKH = matchCollection[row].Groups[7].Value;
                        string diaChiDau = matchCollection[row].Groups[8].Value;
                        string diaChiCuoi = matchCollection[row].Groups[9].Value;
                        string ngayHuy = matchCollection[row].Groups[10].Value;
                        string tinhTrangXacMinh = matchCollection[row].Groups[11].Value;
                        string contents = soThueBao + this.tab + maThueBao + this.tab + dichVu + this.tab + trangThaiTB + this.tab + tinhTrangChanCat + this.tab + loaiSP + this.tab + tenKH + this.tab + diaChiDau + this.tab + diaChiCuoi + this.tab + ngayHuy + this.tab + tinhTrangXacMinh + "\r\n";
                        // xóa dữ liệu nếu đã tồn tại để thực hiện ghi đè

                        file.DeleteTextSeacrhContains("DataFile/danhsachbandau.txt", soThueBao);

                        File.AppendAllText("DataFile/danhsachbandau.txt", contents);
                        flag = true;
                        Thread.Sleep(1000);
                    }
                    if (flag)
                    {
                        this.tbLog.WriteNotify("Lấy thông tin trang: " + (object)(currentPage + 1) + "/" + (object)totalPage + " thành công.");
                    }
                    // nếu số bản ghi của page đó >= 50 và page hiện tại < tổng số page => next page
                    if (matchCollection.Count >= 50 && (currentPage + 1) < totalPage)
                    {
                        firefoxDriver.FindElement(By.CssSelector(".ui-icon-seek-next")).Click();
                        Thread.Sleep(Util.Rand(this.tbDelay.NumValue() * 1000 - this.tbRandomDelay.NumValue() * 1000, this.tbDelay.NumValue() * 1000 + this.tbRandomDelay.NumValue() * 1000));
                    }
                    else
                    {
                        break;
                    }
                }
                this.tbLog.WriteNotify("Done");
                return true;
            }
            catch (Exception ex)
            {
                this.tbLog.WriteNotify("Có lỗi xảy ra");
                _logger.Error(ex);
            }
            return false;
        }

        public bool GetThongTin(string soThueBao)
        {
            int i = 5;
            this.tbLog.WriteNotify("Lấy thông tin khách hàng");
            while (i > 0)
            {
                i--;
                try
                {
                    if (!firefoxDriver.PageSource.Contains("Thông tin khách hàng"))
                    {
                        Thread.Sleep(5000); break;
                    }
                    else
                    {
                        var pageSource = firefoxDriver.PageSource;

                        string maThueBao = this.GetMaThueBao(pageSource);

                        //string maKH = Regex.Match(pageSource, ">Mã khách hàng.*?class=\".*?valuecc.*?\">(.*?)</").Groups[1].Value;
                        //string tenKH = Regex.Match(pageSource, ">Tên khách hàng.*?class=\".*?valuecc.*?\">(.*?)</").Groups[1].Value;
                        //string ngaySinh = Regex.Match(pageSource, ">Ngày sinh.*?class=\".*?valuecc.*?\">(.*?)</").Groups[1].Value;
                        //string ngayCap = Regex.Match(pageSource, ">Ngày cấp.*?class=\".*?valuecc.*?\">(.*?)</").Groups[1].Value;
                        //string noiCap = Regex.Match(pageSource, ">Nơi cấp.*?class=\".*?valuecc.*?\">(.*?)</").Groups[1].Value;
                        //string diaChiKH = Regex.Match(pageSource, ">Địa chỉ khách hàng.*?class=\".*?valuecc.*?\">(.*?)</").Groups[1].Value;
                        //string gioiTinh = Regex.Match(pageSource, ">Giới tính.*?class=\".*?valuecc.*?\">(.*?)</").Groups[1].Value;
                        //string cmt = Regex.Match(pageSource, ">CMT.*?class=\".*?valuecc.*?\">(.*?)</").Groups[1].Value;
                        string dienThoai = Regex.Match(pageSource, ">Điện thoại liên hệ.*?class=\".*?valuecc.*?\">(.*?)</").Groups[1].Value;
                        string ngayDauNoi = Regex.Match(pageSource, ">Ngày đấu nối.*?class=\".*?valuecc.*?\">(.*?)</").Groups[1].Value;
                        string goiCuoc = Regex.Match(pageSource, ">Gói cước.*?class=\".*?ui-commandlink.*?\">(.*?)</", RegexOptions.Singleline).Groups[1].Value.Trim();
                        string trangThaiCat = Regex.Match(pageSource, "Trạng thái chặn cắt.*?class=\".*?ui-widget valuecc_red\">(.*?)</label").Groups[1].Value;
                        string trangThaiThueBao = Regex.Match(pageSource, "Trạng thái thuê bao.*?class=\".*?ui-widget valuecc_red\">(.*?)</label").Groups[1].Value;
                        string diaChiLapDatDau = Regex.Match(pageSource, ">Địa chỉ lắp đặt đầu.*?class=\".*?valuecc.*?\">(.*?)</").Groups[1].Value;
                        string contents = "";
                        MatchCollection matchCollection = Regex.Matches(pageSource, "<span class=\"valuecc\" style=\"margin-left: 22px\" title=\"(.*?)\">(.*?)</span>.*?<span class=\"valuecc\" title=\"(.*?)\">(.*?)</span>", RegexOptions.Singleline);

                        for (int index = 0; index < matchCollection.Count; ++index)
                        {
                            string soThueBaoLQ = matchCollection[index].Groups[2].Value.Trim();
                            string dichVuLQ = matchCollection[index].Groups[4].Value.Trim();
                            contents = contents + soThueBao + this.tab + maThueBao + this.tab + soThueBaoLQ + this.tab + dichVuLQ + "\r\n";
                        }

                        // xóa dữ liệu nếu đã tồn tại để thực hiện ghi đè
                        file.DeleteTextSeacrhContains("DataFile/thongtinkhachhang.txt", soThueBao);

                        //File.AppendAllText("DataFile/thongtinkhachhang.txt", soThueBao + this.tab + maThueBao + this.tab + maKH + this.tab + tenKH + this.tab + ngaySinh + this.tab + cmt + this.tab + ngayCap + this.tab + noiCap + this.tab + diaChiKH + this.tab + gioiTinh + this.tab + dienThoai + this.tab + ngayDauNoi + this.tab + goiCuoc + this.tab + trangThaiCat + this.tab + trangThaiThueBao + this.tab + diaChiLapDatDau + "\r\n");
                        File.AppendAllText("DataFile/thongtinkhachhang.txt", soThueBao + this.tab + maThueBao + this.tab + dienThoai + this.tab + ngayDauNoi + this.tab + goiCuoc + this.tab + trangThaiCat + this.tab + trangThaiThueBao + this.tab + diaChiLapDatDau + "\r\n");
                        if (matchCollection.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(contents.Trim()))
                            {
                                // xóa dữ liệu nếu đã tồn tại để thực hiện ghi đè
                                file.DeleteTextSeacrhContains("DataFile/dichvulienquan.txt", soThueBao);
                                File.AppendAllText("DataFile/dichvulienquan.txt", contents.Trim() + "\r\n");
                            }
                        }
                    }
                    this.tbLog.WriteNotify("Lấy thông tin khách hàng - OK");
                    return true;
                }
                catch (Exception ex)
                {
                    this.tbLog.WriteNotify("Có lỗi xảy ra!");
                    _logger.Error(ex);
                }
                this.tbLog.WriteNotify("Lấy thông tin khách hàng - Repeat");
                Thread.Sleep(2000);
            }
            return false;
        }

        public bool GetCuoc(string soThueBao, string maThueBao)
        {

            int i = 5;
            this.tbLog.WriteNotify("Lấy cước đóng trước/hàng tháng");
            while (i > 0)
            {
                i--;
                try
                {
                    MatchCollection matchCollection = Regex.Matches(firefoxDriver.PageSource, "<tr.*?class=\"ui-widget-content ui-datatable-.*?><td.*?>STT</span>(\\d+)</td><td.*?>Mã CDT</span>(.*?)</td><td.*?>Số tiền đóng trước/tháng</span>(.*?)</td><td.*?>Số tiền CDT ban đầu</span>(.*?)</td><td.*?>Số tiền CDT còn lại</span>(.*?)</td><.*?>Ngày hiệu lực</span>(.*?)</td><td.*?>Ngày hết hạn</span>(.*?)</td><td.*?>Số tháng tặng</span>(.*?)</td><td.*?>Tổng số tiền tặng</span>(.*?)</td><td.*?>Tháng được tặng</span>(.*?)</td><td.*?>Nội dung CDT</span>(.*?)</td><td.*?>Trạng thái</span>(.*?)</td></tr>");

                    if (matchCollection.Count > 0)
                    {
                        string contents = "";
                        for (int index = 0; index < matchCollection.Count; ++index)
                        {
                            string soTT = matchCollection[index].Groups[1].Value;
                            string maCDT = matchCollection[index].Groups[2].Value;
                            string tienDongTruoc = matchCollection[index].Groups[3].Value;
                            string tienCDTBanDau = matchCollection[index].Groups[4].Value;
                            string tienCDTConLai = matchCollection[index].Groups[5].Value;
                            string ngayHieuLuc = matchCollection[index].Groups[6].Value;
                            string ngayHetHan = matchCollection[index].Groups[7].Value;
                            string soThangTang = matchCollection[index].Groups[8].Value;
                            string soTienTang = matchCollection[index].Groups[9].Value;
                            string thangDuocTang = matchCollection[index].Groups[10].Value;
                            string noiDungCDT = matchCollection[index].Groups[11].Value;
                            string trangThai = matchCollection[index].Groups[12].Value;
                            contents += soThueBao + this.tab + maThueBao + this.tab + soTT + this.tab + maCDT + this.tab + tienDongTruoc + this.tab + tienCDTBanDau + this.tab + tienCDTConLai + this.tab + ngayHieuLuc + this.tab + ngayHetHan + this.tab + soThangTang + this.tab + soTienTang + this.tab + thangDuocTang + this.tab + noiDungCDT + this.tab + trangThai + "\r\n";
                        }
                        // xóa dữ liệu nếu đã tồn tại để thực hiện ghi đè
                        file.DeleteTextSeacrhContains("DataFile/cuocdongtruoc.txt", soThueBao);
                        File.AppendAllText("DataFile/cuocdongtruoc.txt", contents);
                    }
                    else
                    {
                        string contents = soThueBao + this.tab + maThueBao + this.tab + "Đóng cước hàng tháng\r\n";

                        // xóa dữ liệu nếu đã tồn tại để thực hiện ghi đè
                        file.DeleteTextSeacrhContains("DataFile/cuocdonghangthang.txt", soThueBao);
                        File.AppendAllText("DataFile/cuocdonghangthang.txt", contents);
                    }

                    this.tbLog.WriteNotify("Lấy cước đóng trước/hàng tháng - OK");
                    return true;
                }
                catch (Exception ex)
                {
                    this.tbLog.WriteNotify("Có lỗi xảy ra!");
                    _logger.Error(ex);
                }

                this.tbLog.WriteNotify("Lấy cước đóng trước/hàng tháng - Repeat");
                Thread.Sleep(2000);
            }
            return false;
        }

        public bool ClickXemCuoc()
        {
            this.tbLog.WriteNotify("Click xem cước");
            int i = 5;
            // loop nếu chưa tìm đc ele thì chạy lại, quá 5 thì break
            while (i > 0)
            {
                i--;
                try
                {
                    firefoxDriver.FindElement(By.XPath("//a[@title='Xem cước đóng trước']")).Click();
                    this.tbLog.WriteNotify("Click xem cước - OK");
                    return true;

                }
                catch (Exception ex)
                {
                    this.tbLog.WriteNotify("Không tìm thấy element!");
                    _logger.Error(ex);
                }
                // chưa pass hoặc exception thì repeat 
                this.tbLog.WriteNotify("Click xem cước - Repeat");
                Thread.Sleep(2000);

            }
            // hết loop mà chưa return thì return false
            return false;
        }

        private string GetMaThueBao(string html)
        {
            try
            {
                return Regex.Match(html, ">Mã thuê bao.*?class=\".*?valuecc.*?\">(.*?)</").Groups[1].Value;
            }
            catch
            {
                return "";
            }
        }

        public bool Navigate(string url)
        {
            this.tbLog.WriteNotify("Thực hiện điều hướng");
            for (int i = 0; i < 2; i++)
            {
                try
                {// navigate home
                    firefoxDriver.Url = url;
                    firefoxDriver.Navigate();
                    this.tbLog.WriteNotify("Thực hiện điều hướng - OK");
                    Thread.Sleep(10000);
                    return true;
                }
                catch (Exception ex)
                {
                    this.tbLog.WriteNotify("Exception timeout!");
                    _logger.Error(ex);
                }
                this.tbLog.WriteNotify("Thực hiện điều hướng - Repeat");
                Thread.Sleep(2000);
            }
            return false;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            firefoxDriver?.Quit();
        }

        private void tbDelay_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != 8 && e.KeyChar != 127 && e.KeyChar != 13 && e.KeyChar != 27)
            {
                e.Handled = true; // Mark the event as handled to prevent the character from being entered.
            }
        }

        private void tbLimit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != 8 && e.KeyChar != 127 && e.KeyChar != 13 && e.KeyChar != 27)
            {
                e.Handled = true; // Mark the event as handled to prevent the character from being entered.
            }
        }

        private void tbRandomDelay_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != 8 && e.KeyChar != 127 && e.KeyChar != 13 && e.KeyChar != 27)
            {
                e.Handled = true; // Mark the event as handled to prevent the character from being entered.
            }

        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            this.tbLog.WriteNotify("Đang xử lý nốt tiến trình còn đang dở và thực hiện Stop chức năng!");
            btnStop.Enabled(false);
            btnSearch.Enabled(true);
            btnInfo.Enabled(true);
        }

        private void btnUpdateService_Click(object sender, EventArgs e)
        {
            try
            {
                var url = "http://10.240.147.246/BCCS_CC/";

                if (!Navigate(url))
                {
                    this.tbLog.WriteNotify("Thực hiện điều hướng - Fail");
                    this.tbLog.WriteNotify("Dừng chức năng!");
                    this.btnSearch.Enabled(true);
                    btnStop.Enabled(false);
                    return;
                }

                if (!ClickMenuTimKiem())
                {
                    this.tbLog.WriteNotify("Click menu tìm kiếm khách hàng - Fail");
                    this.tbLog.WriteNotify("Dừng chức năng!");
                    btnSearch.Enabled(true);
                    return;
                }
                Thread.Sleep(10000);

                this.tbLog.WriteNotify("Cập nhật danh sách dịch vụ");
                var matchCollection = Regex.Matches(firefoxDriver.PageSource, "<label for=\"formIn:l2:cbxService.*?\">(.*?)</label>");
                string contents = "";
                for (int row = 0; row < matchCollection.Count; ++row)
                {
                    contents += matchCollection[row].Groups[1].Value + "\r\n";
                }
                File.WriteAllText("DataFile/service.txt", contents);
                FillCbDichVu();
                this.tbLog.WriteNotify("Cập nhật danh sách dịch vụ - OK");
            }
            catch (Exception ex)
            {
                this.tbLog.WriteNotify("Cập nhật danh sách dịch vụ - Fail");
                _logger.Error(ex);
            }
        }

        public void BindHeaderFile()
        {
            try
            {
                void WriteFile(string path, string header)
                {
                    // xóa dữ liệu nếu đã tồn tại để thực hiện ghi đè
                    file.DeleteTextSeacrhContains(path, header);
                    string existingContent = this.file.Read(path);

                    File.WriteAllText(path, header + existingContent);
                }
                string contents = "";
                string soThueBao = "Số thuê bao";
                string maThueBao = "Mã thuê bao";
                string dichVu = "Dịch vụ";
                string trangThaiTB = "Trạng thái thuê bao";
                string tinhTrangChanCat = "Tình trạng chặn cắt";
                string loaiSP = "Loại SP";
                string tenKH = "Tên KH";
                string diaChiDau = "Địa chỉ đầu";
                string diaChiCuoi = "Địa chỉ cuối";
                string ngayHuy = "Ngày hủy";
                string tinhTrangXacMinh = "Tình trạng xác minh";

                contents = soThueBao + this.tab + maThueBao + this.tab + dichVu + this.tab + trangThaiTB + this.tab + tinhTrangChanCat + this.tab + loaiSP + this.tab + tenKH + this.tab + diaChiDau + this.tab + diaChiCuoi + this.tab + ngayHuy + this.tab + tinhTrangXacMinh + "\r\n";
                WriteFile("DataFile/danhsachbandau.txt", contents);

                string dienThoai = "Điện thoại liên hệ";
                string ngayDauNoi = "Ngày đấu nối";
                string goiCuoc = "Gói cước";
                string trangThaiCat = "Trạng thái chặn cắt";
                string trangThaiThueBao = "Trạng thái thuê bao";
                string diaChiLapDatDau = "Địa chỉ lắp đặt đầu";
                string soThueBaoLQ = "Số thuê bao liên quan";
                string dichVuLQ = "Dịch vụ liên quan";

                contents = soThueBao + this.tab + maThueBao + this.tab + dienThoai + this.tab + ngayDauNoi + this.tab + goiCuoc + this.tab + trangThaiCat + this.tab + trangThaiThueBao + this.tab + diaChiLapDatDau + "\r\n";
                WriteFile("DataFile/thongtinkhachhang.txt", contents);

                contents = soThueBao + this.tab + maThueBao + this.tab + soThueBaoLQ + this.tab + dichVuLQ + "\r\n";
                WriteFile("DataFile/dichvulienquan.txt", contents);


                string soTT = "Số TT";
                string maCDT = "Mã CDT";
                string tienDongTruoc = "Tiền đóng trước";
                string tienCDTBanDau = "Tiền CDT ban đầu";
                string tienCDTConLai = "Tiền CDT còn lại";
                string ngayHieuLuc = "Ngày hiệu lực";
                string ngayHetHan = "Ngày hết hạn";
                string soThangTang = "Số tháng tặng";
                string soTienTang = "Số tiền tặng";
                string thangDuocTang = "Tháng được tặng";
                string noiDungCDT = "Nội dung CDT";
                string trangThai = "Trạng thái";

                contents = soThueBao + this.tab + maThueBao + this.tab + soTT + this.tab + maCDT + this.tab + tienDongTruoc + this.tab + tienCDTBanDau + this.tab + tienCDTConLai + this.tab + ngayHieuLuc + this.tab + ngayHetHan + this.tab + soThangTang + this.tab + soTienTang + this.tab + thangDuocTang + this.tab + noiDungCDT + this.tab + trangThai + "\r\n";
                WriteFile("DataFile/cuocdongtruoc.txt", contents);

                contents = soThueBao + this.tab + maThueBao + this.tab + "Đóng cước hàng tháng\r\n";
                WriteFile("DataFile/cuocdonghangthang.txt", contents);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public void CheckInWorkHour(ref int count, ref int limit)
        {
            // Lấy thời gian hiện tại
            DateTime currentTime = DateTime.Now;

            if (!Util.IsWithinWorkingHours(currentTime))
            {
                this.tbLog.WriteNotify("Ngoài giờ làm việc.");
                // Xử lý khi ngoài giờ làm việc
                DateTime nextWorkingTime = Util.CalculateNextWorkingTime(currentTime);
                TimeSpan sleepDuration = nextWorkingTime - currentTime;
                string formattedDateTime = nextWorkingTime.ToString("dd/MM/yyyy HH:mm:ss tt");
                this.tbLog.WriteNotify("Chờ đến " + formattedDateTime + " để tiếp tục làm việc - tương đương " + String.Format("{0:0.00}", sleepDuration.TotalMinutes) + " phút.");
                if (nextWorkingTime == currentTime.Date.AddDays(1).Add(new TimeSpan(8, 0, 0)))
                {
                    count = 0; 
                    limit = Util.Rand(Int32.Parse(this.tbLimit.Text) - 10, Int32.Parse(this.tbLimit.Text) + 10);
                }
                btnStop.Enabled(false);
                Thread.Sleep(sleepDuration);
                btnStop.Enabled(true);
            }
            return;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                firefoxDriver?.Quit();

                btnLogin.Enabled(true);
                btnSearch.Enabled(false);
                btnInfo.Enabled(false);
                btnStop.Enabled(false);
                btnUpdateService.Enabled(false);
                btnReset.Enabled(false);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                this.tbLog.WriteNotify("Không khởi tạo được driver.");
                btnLogin.Enabled(true);
                return;
            }
            MessageBox.Show("Đã làm mới ứng dụng.");
        }

        private void btnResetFile_Click(object sender, EventArgs e)
        {
            int selectedValue = (int)this.cbFile.SelectedValue;
            string selectedText = this.cbFile.Text;
            string filename = "danhsachbandau";
            try
            {
                switch (selectedValue)
                {
                    case 1: filename = "danhsachbandau"; break;
                    case 2: filename = "thongtinkhachhang"; break;
                    case 3: filename = "dichvulienquan"; break;
                    case 4: filename = "cuocdongtruoc"; break;
                    case 5: filename = "cuocdonghangthang"; break;
                    case 6: filename = "danhsachdacheck"; break;
                    default: break;
                }
                File.WriteAllText($"DataFile/{filename}.txt", "");
                this.tbLog.WriteNotify($"Reset file \"{selectedText}\" thành công.");
                BindHeaderFile();
            }
            catch (Exception ex)
            {
                this.tbLog.WriteNotify($"Reset file \"{selectedText}\" không thành công.");
                _logger.Error(ex);
            }
        }

        public void InitCheckedFile()
        {
            void NewData()
            {

                string delay = "180";
                string randomDelay = "20";
                string countChecked = "0";
                string limit = "200";
                string today = DateTime.Today.ToString("dd/MM/yyyy");
                string value = $"Delay:{this.tab}{delay}\nRandomDelay:{this.tab}{randomDelay}\nCountChecked:{this.tab}{countChecked}\nLimit:{this.tab}{limit}\nDatetime:{this.tab}{today}\n";
                this.tbDelay.Text = delay;
                this.tbRandomDelay.Text = randomDelay;
                this.tbLimit.Text = limit;
                this.lbChecked.WriteLbCheck(countChecked);
                File.WriteAllText("checkconfig.cfg", value);
            }

            try
            {
                if (File.Exists("checkconfig.cfg"))
                {
                    string dataFile = this.file.Read("checkconfig.cfg");
                    var math = Regex.Match(dataFile, "Delay:\t(\\d+)\nRandomDelay:\t(\\d+)\nCountChecked:\t(\\d+)\nLimit:\t(\\d+)\nDatetime:\t(.*?)\n");
                    if (math.Success)
                    {
                        string delay = math.Groups[1].Value.Trim();
                        string randomDelay = math.Groups[2].Value.Trim();
                        string countChecked = math.Groups[3].Value.Trim();
                        string limit = math.Groups[4].Value.Trim();
                        string oldDate = math.Groups[5].Value.Trim();
                        DateTime parsedDate = DateTime.ParseExact(oldDate, "dd/MM/yyyy", null);
                        DateTime today = DateTime.Today;
                        if (today > parsedDate)
                        {
                            countChecked = "0";
                        }
                        this.tbDelay.Text = delay;
                        this.tbRandomDelay.Text = randomDelay;
                        this.tbLimit.Text = limit;
                        this.lbChecked.WriteLbCheck(countChecked);
                        string value = $"Delay:{this.tab}{delay}\nRandomDelay:{this.tab}{randomDelay}\nCountChecked:{this.tab}{countChecked}\nLimit:{this.tab}{limit}\nDatetime:{this.tab}{today.ToString("dd/MM/yyyy")}\n";
                        File.WriteAllText("checkconfig.cfg", value);
                    }
                    else
                    {
                        NewData();
                    }
                }
                else
                {
                    NewData();
                }


            }
            catch (Exception ex)
            {
                NewData();
                _logger.Error(ex);
            }
        }
    }
}
