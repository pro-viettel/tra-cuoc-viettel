﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viettel.GetData.Configs
{
    public class AppConfig
    {
        public static string APP_NAME = "Tra cước Viettel";
        public static string APP_CODE = "Tra_cuoc_Viettel";
        public static string APP_VERSION = "v1.0.2";
    }
}
